package impl.org.controlsfx.tableview2.filter.parser;

import impl.org.controlsfx.tableview2.filter.parser.number.NumberParser;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ParserTest extends Application {
    @Override
    public void start(Stage primaryStage) {
        TextField textField = new TextField();
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            textField.setStyle(null);
            if (newValue.length() > 1) {
                try {
                    System.out.println(new NumberParser<>().parse(newValue).test(10));
                } catch (Exception e) {
                    e.printStackTrace();
                    textField.setStyle("-fx-background-color: red");
                }
            }
        });
        StackPane stackPane = new StackPane(textField);
        Scene scene = new Scene(stackPane, 400, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
