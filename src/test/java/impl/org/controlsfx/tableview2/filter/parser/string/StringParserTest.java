package impl.org.controlsfx.tableview2.filter.parser.string;

import impl.org.controlsfx.tableview2.filter.parser.string.StringParser;
import javafx.util.StringConverter;
import org.junit.Assert;
import org.junit.Test;

public class StringParserTest {

    @Test
    public void beginsWith() {
        Assert.assertTrue(new StringParser<>().parse("begins with \"TableView\"").test("TableView is a special node"));
    }

    @Test
    public void doesNotBeginWith() {
        Assert.assertFalse(new StringParser<>().parse("begins with \"is\"").test("TableView is a special node"));
    }

    @Test
    public void contains() {
        Assert.assertTrue(new StringParser<>().parse("contains \"node\"").test("TableView is a special node"));
    }

    @Test
    public void doesNotContains() {
        Assert.assertFalse(new StringParser<>().parse("contains \"node\"").test("TableView is a special control"));
    }

    @Test
    public void endsWith() {
        Assert.assertTrue(new StringParser<>().parse("ends with \"node\"").test("TableView is a special node"));
    }

    @Test
    public void doesNotEndsWith() {
        Assert.assertFalse(new StringParser<>().parse("ends with \"node\"").test("TableView is a special control"));
    }

    @Test
    public void caseSensitive() {
        Assert.assertFalse(new StringParser<>(true).parse("ends with \"Node\"").test("TableView is a special node"));
    }

    @Test
    public void caseNull() {
        Assert.assertFalse(new StringParser<>(true, stringConverter()).parse("ends with \"Node\"").test(null));
    }

    @Test
    public void objectWithStringConverter() {
        Person abhinay = new Person("Abhinay", 27);
        StringParser<Person> personStringParser = new StringParser<>(false, new StringConverter<Person>() {
            @Override
            public String toString(Person object) {
                return object.getName();
            }

            @Override
            public Person fromString(String string) {
                return null;
            }
        });
        Assert.assertTrue(personStringParser.parse("begins with \"abh\"").test(abhinay));
        Assert.assertTrue(personStringParser.getErrorMessage().isEmpty());
    }


    private static <T> StringConverter<T> stringConverter() {
        return new StringConverter<T>() {
            @Override public String toString(T t) {
                return t == null ? null : t.toString();
            }

            @Override public T fromString(String string) {
                return (T) string;
            }
        };
    }
    
    class Person {

        String name;
        int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "$1" + name + age;
        }
    }
}
