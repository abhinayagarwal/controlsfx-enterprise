package impl.org.controlsfx.tableview2.filter.parser;

import impl.org.controlsfx.tableview2.filter.parser.number.NumberParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ORParserTest {

    private NumberParser<Integer> parser;

    @Before
    public void setUp() {
        parser = new NumberParser<>();
    }

    @Test
    public void isEqualsOrEquals() {
        Assert.assertTrue(parser.parse("= 100 OR = 110").test(110));
    }

    @Test
    public void isGreaterThanEqualsOrEquals() {
        Assert.assertTrue(parser.parse("≥ 120 OR = 110").test(120));
    }
}
