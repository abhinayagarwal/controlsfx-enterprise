package org.controlsfx.control.tableview2;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

public class Person {

    private final StringProperty firstName = new SimpleStringProperty();
    private final StringProperty lastName = new SimpleStringProperty();
    private final IntegerProperty age = new SimpleIntegerProperty();
    private final StringProperty streetAddress = new SimpleStringProperty();
    private final StringProperty city = new SimpleStringProperty();
    private final StringProperty zip = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final BooleanProperty active = new SimpleBooleanProperty();
    private final ObjectProperty<LocalDate> birthday = new SimpleObjectProperty<>();
    private final ObjectProperty<Color> color = new SimpleObjectProperty<>();

    public final LocalDate getBirthday() {
        return birthday.get();
    }

    public final void setBirthday(LocalDate value) {
        birthday.set(value);
    }

    public final ObjectProperty<LocalDate> birthdayProperty() {
        return birthday;
    }
    

    public final StringProperty firstNameProperty() {
        return this.firstName;
    }

    public final java.lang.String getFirstName() {
        return this.firstNameProperty().get();
    }

    public final void setFirstName(final java.lang.String firstName) {
        this.firstNameProperty().set(firstName);
    }

    public final StringProperty lastNameProperty() {
        return this.lastName;
    }

    public final java.lang.String getLastName() {
        return this.lastNameProperty().get();
    }

    public final void setLastName(final java.lang.String lastName) {
        this.lastNameProperty().set(lastName);
    }

    public final StringProperty streetAddressProperty() {
        return this.streetAddress;
    }

    public final java.lang.String getStreetAddress() {
        return this.streetAddressProperty().get();
    }

    public final void setStreetAddress(final java.lang.String streetAddress) {
        this.streetAddressProperty().set(streetAddress);
    }

    public final StringProperty cityProperty() {
        return this.city;
    }

    public final java.lang.String getCity() {
        return this.cityProperty().get();
    }

    public final void setCity(final java.lang.String city) {
        this.cityProperty().set(city);
    }

    public final StringProperty zipProperty() {
        return this.zip;
    }

    public final java.lang.String getZip() {
        return this.zipProperty().get();
    }

    public final void setZip(final java.lang.String zip) {
        this.zipProperty().set(zip);
    }

    public final StringProperty emailProperty() {
        return this.email;
    }

    public final java.lang.String getEmail() {
        return this.emailProperty().get();
    }

    public final void setEmail(final java.lang.String email) {
        this.emailProperty().set(email);
    }

    public final BooleanProperty activeProperty() {
        return this.active;
    }

    public final boolean isActive() {
        return this.activeProperty().get();
    }

    public final void setActive(final boolean active) {
        this.activeProperty().set(active);
    }

    public final int getAge() {
        return age.get();
    }

    public final IntegerProperty ageProperty() {
        return age;
    }

    public final void setAge(int age) {
        this.age.set(age);
    }
    
    public final Color getColor() {
        return color.get();
    }

    public final void setColor(Color value) {
        color.set(value);
    }

    public final ObjectProperty<Color> colorProperty() {
        return color;
    }

    public Person (String firstName, String lastName, Integer age, String address,
                   String city, String zip, boolean active, LocalDate birthday, Color color){
        this.firstName.set(firstName);
        this.lastName.set(lastName);
        this.age.set(age);
        this.streetAddress.set(address);
        this.city.set(city);
        this.zip.set(zip);
        this.active.set(active);
        this.birthday.set(birthday);
        this.color.set(color);
    }

    @Override
    public String toString() {
        return /*"Person{" + */"firstName=" + firstName.get() /*+ ", lastName=" + lastName.get() + 
                ", streetAddress=" + streetAddress.get() + ", city=" + city.get() + 
                ", zip=" + zip.get() + ", email=" + email.get() + ", active=" + active.get() + 
                ", birthday=" + birthday.get()+ '}'*/;
    }

    public Person() {
        this.firstName.set("");
        this.lastName.set("");
        this.age.set(18);
        this.streetAddress.set("");
        this.city.set("");
        this.zip.set("");
        this.active.set(false);
        this.birthday.set(LocalDate.now());
        this.color.set(Color.CADETBLUE);
    }
    
    public static ObservableList<Person> generateData(int numberOfPeople){
        ObservableList<Person> persons = FXCollections.observableArrayList(e -> new Observable[]{ e.lastNameProperty() });
        
        List<Color> colors = Arrays.asList(Color.CADETBLUE, Color.CHARTREUSE, Color.CHOCOLATE, Color.CORAL, Color.CORNSILK, Color.CORNFLOWERBLUE);
        for (int i = 0; i < numberOfPeople; i++) {
            persons.add(new Person("First Name:  " + i%20, "Last Name: " + i%10, 18 + i, "I Live Here: " + i%5,
                    "City: " + i%3, "Zip: " + i, i%10 != 0, LocalDate.of(1900+i, 1+i%11, 1+i%29), colors.get(new Random().nextInt(colors.size()))));
        }
        
        return persons;
    }
    
    public IntegerProperty getTotalLength() {
        IntegerProperty length = new SimpleIntegerProperty();
        length.bind(Bindings.createIntegerBinding(() -> getFirstName().length() + getLastName().length() + getCity().length() + getStreetAddress().length() + getZip().length(), 
                firstName, lastName, city, streetAddress, zip));
        return length;
    }
    
    public IntegerProperty getTotalSum() {
        IntegerProperty sum = new SimpleIntegerProperty();
        sum.bind(Bindings.createIntegerBinding(() -> getNumberOf(getFirstName()) + getNumberOf(getLastName()) + getNumberOf(getCity()) + getNumberOf(getStreetAddress()) + getNumberOf(getZip()), 
                firstName, lastName, city, streetAddress, zip));
        return sum;
    }
    
    private int getNumberOf(String text) {
        try {
            final String[] split = text.split(":");
            if (split.length == 2) {
                return Integer.parseInt(split[1].trim());
            }
        } catch (NumberFormatException e) {}
        return 0;
    }
}
