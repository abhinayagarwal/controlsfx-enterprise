package org.controlsfx.control.tableview2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class Test1TableView2 extends Application {
    private final ObservableList<Person> data = Person.generateData(100);
    
    private final TableView2<Person> table = new TableView2<>();
    private final TableColumn<Person, String> firstName = new TableColumn<>("First Name");
    private final TableColumn<Person, String> lastName = new TableColumn<>("Last Name");
    private final TableColumn<Person, String> city = new TableColumn<>("City");
    private final TableColumn<Person, LocalDate> birthday = new TableColumn<>("Birthday");
    private final TableColumn<Person, Boolean> active = new TableColumn<>("Active");
    
    @Override
    public void start(Stage primaryStage) {
        firstName.setCellValueFactory(p -> p.getValue().firstNameProperty());
        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        firstName.setPrefWidth(110);
        
        lastName.setCellValueFactory(p -> p.getValue().lastNameProperty());
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setPrefWidth(130);
        
        city.setCellValueFactory(p -> p.getValue().cityProperty());
        city.setCellFactory(TextFieldTableCell.forTableColumn());
        
        birthday.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthday.setPrefWidth(100);
        birthday.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        birthday.setEditable(false);
        
        active.setText("Active");
        active.setCellValueFactory(p -> p.getValue().activeProperty());
        active.setCellFactory(CheckBoxTableCell.forTableColumn(active));
        active.setPrefWidth(60);
        
        table.setItems(data);
        TableColumn fullNameColumn = new TableColumn("Full Name");
        fullNameColumn.getColumns().addAll(firstName, lastName);
        table.getColumns().addAll(fullNameColumn, city, birthday, active);
        table.setEditable(true);
        
        Scene scene = new Scene(table, 400, 500);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
       
        table.getFixedRows().addAll(0, 1, 2);
        table.getFixedColumns().add(fullNameColumn);

        table.setRowHeaderVisible(true);
//        table.getSelectionModel().setCellSelectionEnabled(true);
//        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
